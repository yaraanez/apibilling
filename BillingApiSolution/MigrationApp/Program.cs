﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using BillingApi.DataAccess.Entities;
using Generic.DataTransfer.Enums;

namespace MigrationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new Context())
            {
                if (!context.Database.EnsureCreated())
                {
                    Console.WriteLine("La base de datos ya existe!");
                    string[] pendingMigrations = context.Database.GetPendingMigrations().ToArray();
                    if (pendingMigrations.Any())
                    {
                        string tmpMigration = "";
                        if (pendingMigrations.Contains(tmpMigration)) // por cada migración con seeder
                        {

                            Console.WriteLine($"Seeder ejecutado para migración \"{tmpMigration}\"!");
                        }
                        context.Database.Migrate();
                        Console.WriteLine("Migraciones ejecutadas exitosamente!");
                    }
                }
                else
                {
                    Console.WriteLine("Se ha creado la base de datos correctamente!");                  

                    context.IdentityDocumentType.AddRange(
                        new IdentityDocumentType()
                        {
                            IdentityDocumentTypeId = 1,
                            Name = "CI",
                            Descript = "Carnet Identidad",
                            Title = "Carnet Identidad",
                            KeySin = "1",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new IdentityDocumentType()
                        {
                            IdentityDocumentTypeId = 2,
                            Name = "Carnet de Recidente",
                            Descript = "Carnet de Recidente",
                            Title = "Carnet de Recidente",
                            KeySin = "2",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        }, 
                        new IdentityDocumentType()
                        {
                            IdentityDocumentTypeId = 3,
                            Name = "Pasaporte",
                            Descript = "Pasaporte",
                            Title = "Pasaporte",
                            KeySin = "3",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new IdentityDocumentType()
                        {
                            IdentityDocumentTypeId = 4,
                            Name = "Otro",
                            Descript = "Otro",
                            Title = "Otro",
                            KeySin = "4",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new IdentityDocumentType()
                        {
                            IdentityDocumentTypeId = 5,
                            Name = "NIT",
                            Descript = "Número de Identificación Tributaria",
                            Title = "NIT",
                            KeySin = "5",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        });

                    context.EconomicActivityType.AddRange(
                        new EconomicActivityType()
                        {
                            EconomicActivityId = 1,
                            Name = "Farmacia",
                            Descript = "Farmacia",
                            Leyend = "Venta Al Por Menor de Productos Farmaceuticos, Medicinales Y Otros En Almacen.",
                            KeySin = "477311",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new EconomicActivityType()
                        {
                            EconomicActivityId = 2,
                            Name = "Sala",
                            Descript = "Sala",
                            Leyend = "Venta Al Por Menor de una variedad de productos que no reflejan una especialidad (Supermercados, Distribuidores y otros).",
                            KeySin = "471110",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        });

                    context.FiscalDocumentType.AddRange(
                        new FiscalDocumentType()
                        {
                            FiscalDocumentTypeId = 1,
                            Name = "Factura",
                            Title = "Factura",
                            Descript = "Factura",
                            GroupKey = "F",
                            KeySin = "1",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new FiscalDocumentType()
                        {
                            FiscalDocumentTypeId = 2,
                            Name = "Nota de Crédito / Débito",
                            Title = "Nota de Crédito / Débito",
                            Descript = "Nota de Crédito / Débito",
                            GroupKey = "D",
                            KeySin = "2",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new FiscalDocumentType()
                        {
                            FiscalDocumentTypeId = 3,
                            Name = "Nota Fiscal",
                            Title = "Nota Fiscal",
                            Descript = "Nota Fiscal",
                            GroupKey = "F",
                            KeySin = "3",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new FiscalDocumentType()
                        {
                            FiscalDocumentTypeId = 4,
                            Name = "Boleta Contingencia",
                            Title = "Boleta Contingencia",
                            Descript = "Boleta Contingencia",
                            GroupKey = "F",
                            KeySin = "4",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new FiscalDocumentType()
                        {
                            FiscalDocumentTypeId = 5,
                            Name = "Documento Equivalente",
                            Title = "Documento Equivalente",
                            Descript = "Documento Equivalente",
                            GroupKey = null,
                            KeySin = "5",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        });

                    context.SaleState.AddRange(
                        new SaleState()
                        {
                            SaleStateId = 0,
                            Name = "Pendiente",
                            Descript = "Pendiente",
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new SaleState()
                        {
                            SaleStateId = 1,
                            Name = "Facturada",
                            Descript = "Facturada",
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new SaleState()
                        {
                            SaleStateId = 2,
                            Name = "Anulada",
                            Descript = "Anulada",
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        });

                    context.Dosage.AddRange(
                        new Dosage()
                        {
                            AutorizationNumber = 371401800139951,
                            Name = "FACT0000001",
                            DosageKey = "wX$@m75V57%=86Upp%+L8926%LgW#KJ$\\]kU*5[LJ@c8#TIFaDS@[6fS5c3X$WXU",
                            EconomicActivityId = 1,
                            GroupKey = "F",
                            EconomicActivityLeyend = "Ley Nro.453:Los alimentos declarados de primera necesidad deben ser suministrados de manera adecuada, oportuna, continua y a precio justo.",
                            DeadlineDate = new DateTime(2020, 8, 1),
                            InitialInvoiceNumber = 1,
                            InvoiceNumber = 1,
                            Management = 2020,
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new Dosage()
                        {
                            AutorizationNumber = 371401800139952,
                            Name = "FACT0000B102",
                            DosageKey = "*n_g%@pDgjL%%9$Z)Tq($M#vZ*Dc+y9bIZHA6[%kZQ9A8c[E\\NAsF*D)hTeu=]cF",
                            EconomicActivityId = 1,
                            GroupKey = "D",
                            EconomicActivityLeyend = "Ley Nro.453:Los alimentos declarados de primera necesidad deben ser suministrados de manera adecuada, oportuna, continua y a precio justo.",
                            DeadlineDate = new DateTime(2020, 8, 1),
                            InitialInvoiceNumber = 1,
                            InvoiceNumber = 1,
                            Management = 2020,
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new Dosage()
                        {
                            AutorizationNumber = 371401800139953,
                            Name = "FACT0000B103",
                            DosageKey = "wX$@m75V57%=86Upp%+L8926%LgW#KJ$\\]kU*5[LJ@c8#TIFaDS@[6fS5c3X$WXU",
                            EconomicActivityId = 2,
                            GroupKey = "F",
                            EconomicActivityLeyend = "Ley Nro.453:Los alimentos declarados de primera necesidad deben ser suministrados de manera adecuada, oportuna, continua y a precio justo.",
                            DeadlineDate = new DateTime(2020, 8, 1),
                            InitialInvoiceNumber = 1,
                            InvoiceNumber = 1,
                            Management = 2020,
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new Dosage()
                        {
                            AutorizationNumber = 371401800139954,
                            Name = "FACT0000B104",
                            DosageKey = "*n_g%@pDgjL%%9$Z)Tq($M#vZ*Dc+y9bIZHA6[%kZQ9A8c[E\\NAsF*D)hTeu=]cF",
                            EconomicActivityId = 2,
                            GroupKey = "D",
                            EconomicActivityLeyend = "Ley Nro.453:Los alimentos declarados de primera necesidad deben ser suministrados de manera adecuada, oportuna, continua y a precio justo.",
                            DeadlineDate = new DateTime(2020, 8, 1),
                            InitialInvoiceNumber = 1,
                            InvoiceNumber = 1,
                            Management = 2020,
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,                              
                        });

                    context.SaleDetailType.AddRange(
                        new SaleDetailType() {
                            SaleDetailTypeId = 1,
                            Name = "Venta",
                            Descript = "Venta",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        },
                        new SaleDetailType()
                        {
                            SaleDetailTypeId = 2,
                            Name = "Devolución",
                            Descript = "Devolución",
                            State = StateEnum.Enabled,
                            CreateDate = DateTime.Now,
                            EditDate = DateTime.Now,
                        });

                    context.SaveChanges();

                    Console.WriteLine("Ejecución de seeder exitosa!");
                }
            }
            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
