﻿using BillingApi.DataAccess.Entities;
using BillingApi.DataAccess.Mapping;
using BillingApi.DataAccess.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrationApp
{
    public class Context : DbContext, IBillingUnitOfWork
    {
        public Context() : base()
        {
        }

        public virtual DbSet<Dosage> Dosage { get; set; }
        public virtual DbSet<EconomicActivityType> EconomicActivityType { get; set; }
        public virtual DbSet<FiscalDocument> FiscalDocument { get; set; }
        public virtual DbSet<FiscalDocumentCreDe> FiscalDocumentCreDe { get; set; }
        public virtual DbSet<FiscalDocumentType> FiscalDocumentType { get; set; }
        public virtual DbSet<IdentityDocumentType> IdentityDocumentType { get; set; }
        public virtual DbSet<Sale> Sale { get; set; }
        public virtual DbSet<SaleDetail> SaleDetail { get; set; }
        public virtual DbSet<SaleDetailType> SaleDetailType { get; set; }
        public virtual DbSet<SaleDiscount> SaleDiscount { get; set; }
        public virtual DbSet<SaleLineDiscount> SaleLineDiscount { get; set; }
        public virtual DbSet<SalePayment> SalePayment { get; set; }
        public virtual DbSet<SaleState> SaleState { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(
                @"Server=.;User ID=user;Password=user;Database=dbBilling;");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DosageConfig());
            modelBuilder.ApplyConfiguration(new EconomicActivityTypeConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentCreDeConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentTypeConfig());
            modelBuilder.ApplyConfiguration(new IdentityDocumentTypeConfig());
            modelBuilder.ApplyConfiguration(new SaleStateConfig());
            modelBuilder.ApplyConfiguration(new SaleConfig());
            modelBuilder.ApplyConfiguration(new SaleDetailConfig());
            modelBuilder.ApplyConfiguration(new SaleDetailTypeConfig());
            modelBuilder.ApplyConfiguration(new SalePaymentConfig());
            modelBuilder.ApplyConfiguration(new SaleDiscountConfig());
            modelBuilder.ApplyConfiguration(new SaleLineDiscountConfig());
        }
    }
}
