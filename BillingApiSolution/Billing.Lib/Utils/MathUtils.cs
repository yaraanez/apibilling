﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.Lib.Utils
{
    public class MathUtils
    {
        public static decimal Round(decimal val, decimal dec)
        {
            decimal num1 = val * dec;
            long num2 = (long)num1;
            if (num1 - num2 >= (decimal)0.5)
            {
                ++num2;
            }                
            return (decimal)num2 / dec;
        }
    }
}
