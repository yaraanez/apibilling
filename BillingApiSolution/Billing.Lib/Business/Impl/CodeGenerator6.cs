﻿using Billing.Lib.Cripto;
using Billing.Lib.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Billing.Lib.Business.Impl
{
    public class CodeGenerator6 : CodeGenerator
    {        
        public override string GenerateCode(GenerateCodeParams input, bool logs)
        {
            long num1 = CryptoVerhoeff.AppendCheckDigit(input.AuthorizationNumber);
            long num2 = CryptoVerhoeff.AppendCheckDigit(input.InvoiceNumber);
            long num3 = CryptoVerhoeff.AppendCheckDigit(input.CiNitClient);
            long num4 = CryptoVerhoeff.AppendCheckDigit(input.TransactionDate);
            long num5 = CryptoVerhoeff.AppendCheckDigit(input.Amount);                            
            Base64 base64 = Base64.ReadInt((long)(int)((num1 + num2 + num3 + num4 + num5) % 1073741823L));
            return AllegedRC4.Bin2hex(AllegedRC4.Encrypt(input.Key, base64.ToString(), false));
        }
    }
}
