﻿using Billing.Lib.Cripto;
using Billing.Lib.DataTransfer;
using Billing.Lib.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.Lib.Business.Impl
{
    public class CodeGenerator7: CodeGenerator
    {        
        public CodeGenerator7():base()
        {
        }

        public override string GenerateCode(GenerateCodeParams input, bool logs)
        {
            // log.Info("Paso 1 facturacion electronica");
            long num1 = CryptoVerhoeff.AppendCheckDigit(CryptoVerhoeff.AppendCheckDigit(input.AuthorizationNumber));
            long num2 = CryptoVerhoeff.AppendCheckDigit(CryptoVerhoeff.AppendCheckDigit(input.InvoiceNumber));
            string str1 = CryptoVerhoeff.AppendCheckDigit(CryptoVerhoeff.AppendCheckDigit(input.CiNitClient.ToString()));
            long num3 = CryptoVerhoeff.AppendCheckDigit(CryptoVerhoeff.AppendCheckDigit(input.TransactionDate));
            string num4 = CryptoVerhoeff.AppendCheckDigit(CryptoVerhoeff.AppendCheckDigit("" + input.Amount));
            string me = "Aut:" + num1 + "|Fact:" + num2 + "|Nit:" + str1 + "|Fecha:" + num3 + "|Monto:" + num4;
            //   log.Info(me);
            long input1 = num2 + Convert.ToInt64(str1) + num3 + long.Parse(num4);
            //  log.Info("Paso 1: Suma: " + input1);
            int[] numArray = new int[5];
            for (int index = 0; index < 5; ++index)
            {
                input1 = CryptoVerhoeff.AppendCheckDigit(input1);
                numArray[index] = (int)(input1 % 10L);
            }
            var str2 = string.Empty;
            for (int index = 0; index < 5; ++index)
            {
                var caracter = numArray[index];
                str2 += caracter;
            }
            int startIndex1 = 0;
            string str3;
            string str4;
            string str5;
            string str6;
            string str7;
            try
            {
                str3 = input.AuthorizationNumber + input.Key.Substring(startIndex1, numArray[0] + 1);
                int startIndex2 = startIndex1 + (numArray[0] + 1);
                str4 = num2 + input.Key.Substring(startIndex2, numArray[1] + 1);
                int startIndex3 = startIndex2 + (numArray[1] + 1);
                str5 = str1 + input.Key.Substring(startIndex3, numArray[2] + 1);
                int startIndex4 = startIndex3 + (numArray[2] + 1);
                str6 = num3 + input.Key.Substring(startIndex4, numArray[3] + 1);
                int startIndex5 = startIndex4 + (numArray[3] + 1);
                str7 = num4 + input.Key.Substring(startIndex5, numArray[4] + 1);
                int num5 = startIndex5 + (numArray[4] + 1);
            }
            catch (Exception ex)
            {
                throw new AlgorithmException();
            }
            /*log.Info("Paso 2 Nro Autorizacion:" + str3);
            log.Info("Paso 2 Nro Autorizacion:" + str4);
            log.Info("Paso 2 Ci NIT:" + str5);
            log.Info("Paso 2 Fecha:" + str6);
            log.Info("Paso 2 Monto:" + str7);*/
            if (logs)
            {
                Console.WriteLine("PASO 3");
                Console.WriteLine("====================");
            }
            string data1 = str3 + str4 + str5 + str6 + str7;
            string pwd1 = input.Key + numArray[0] + numArray[1] + numArray[2] + numArray[3] + numArray[4];
            if (logs)
            {
                Console.WriteLine("PWD AllegedRC4: " + pwd1);
                Console.WriteLine("DATA AllegedRC4: " + data1);
            }
            string str8 = AllegedRC4.Bin2hex(AllegedRC4.Encrypt(pwd1, data1, false)).ToUpper();
            if (logs)
                Console.WriteLine("AllegedRC4: " + str8);
            if (logs)
            {
                Console.WriteLine("PASO 4");
                Console.WriteLine("====================");
            }
            char[] chArray = str8.ToCharArray();
            int num6 = 0;
            int num7 = 0;
            int num8 = 0;
            int num9 = 0;
            int num10 = 0;
            int num11 = 0;
            for (int index = 0; index < chArray.Length; ++index)
            {
                int num5 = Convert.ToInt32(chArray[index]);
                num6 += num5;
                if (index % 5 == 0)
                    num7 += num5;
                if (index % 5 == 1)
                    num8 += num5;
                if (index % 5 == 2)
                    num9 += num5;
                if (index % 5 == 3)
                    num10 += num5;
                if (index % 5 == 4)
                    num11 += num5;
            }
            if (logs)
            {
                Console.WriteLine();
                Console.WriteLine(num6);
                Console.WriteLine(num7);
                Console.WriteLine(num8);
                Console.WriteLine(num9);
                Console.WriteLine(num10);
                Console.WriteLine(num11);
            }
            if (logs)
            {
                Console.WriteLine("PASO 5");
                Console.WriteLine("====================");
            }
            long num12 = num6 * num7 / (numArray[0] + 1);
            long num13 = num6 * num8 / (numArray[1] + 1);
            long num14 = num6 * num9 / (numArray[2] + 1);
            long num15 = num6 * num10 / (numArray[3] + 1);
            long num16 = num6 * num11 / (numArray[4] + 1);
            Base64 base64 = Base64.ReadInt(num12 + num13 + num14 + num15 + num16);
            if (logs)
            {
                Console.WriteLine(num12);
                Console.WriteLine(num13);
                Console.WriteLine(num14);
                Console.WriteLine(num15);
                Console.WriteLine(num16);
                Console.WriteLine(base64.ToString());
            }
            if (logs)
            {
                Console.WriteLine("PASO 6");
                Console.WriteLine("====================");
            }
            string pwd2 = pwd1;
            string data2 = base64.ToString();
            string str9 = AllegedRC4.Bin2hex(AllegedRC4.Encrypt(pwd2, data2, false));
            if (logs)
            {
                Console.WriteLine(pwd2);
                Console.WriteLine(data2);
                Console.WriteLine(str9);
            }
            return str9;
        }
    }
}
