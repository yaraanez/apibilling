﻿using Billing.Lib.DataTransfer;
using System.Text.RegularExpressions;

namespace Billing.Lib.Business.Impl
{
    public abstract class CodeGenerator : IControlCodeGenerator
    {
        public const long SixtyFourExp5minus1 = 1073741823;

        public CodeGenerator()
        {
        }

        public abstract string GenerateCode(GenerateCodeParams input, bool logs);        

        public static CodeGenerator GetInstance(CodeVersion version)
        {
            CodeGenerator response = null;
            switch (version)
            {
                case CodeVersion.Normal:
                    response = new CodeGenerator6();
                    break;
                case CodeVersion.Qr:
                    response = new CodeGenerator7();
                    break;
                default:
                    response = new CodeGenerator7();
                    break;
            }
            return response;
        }       
    }
}
