﻿using Billing.Lib.Utils;
using System;

namespace Billing.Lib.DataTransfer
{
    public class GenerateCodeParams
    {
        public long AuthorizationNumber { get; set; }
        public long InvoiceNumber { get; set; }
        public long CiNitClient { get; set; }
        public long TransactionDate { get; set; }
        public long Amount { get; set; }
        public string Key { get; set; }

        public decimal AmountDouble
        {
            get
            {
                return (decimal)this.Amount;
            }
            set
            {
                decimal num1 = MathUtils.Round(MathUtils.Round(MathUtils.Round(value, (decimal)10000.0), (decimal)1000.0), (decimal)100.0);
                long num2 = (long)num1;
                if (num1 - (decimal)num2 >= (decimal)0.5)
                {
                    ++num2;
                }
                this.Amount = num2;
            }
        }

        public DateTime TransactionDateTime
        {
            get
            {
                string str = this.TransactionDate.ToString() + "";
                return new DateTime(Convert.ToInt32(str.Substring(0, 4)), Convert.ToInt32(str.Substring(4, 2)), Convert.ToInt32(str.Substring(6, 2)));
            }
            set
            {
                this.TransactionDate = Convert.ToInt64(value.Year.ToString() + "" + (value.Month.ToString() + "").PadLeft(2, '0') + (value.Day.ToString() + "").PadLeft(2, '0'));
            }
        }
    }
}
