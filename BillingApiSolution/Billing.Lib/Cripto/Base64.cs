﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.Lib.Cripto
{
    public class Base64
    {
        public static char[] Signs = new char[64]
        {
      '0',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
      'V',
      'W',
      'X',
      'Y',
      'Z',
      'a',
      'b',
      'c',
      'd',
      'e',
      'f',
      'g',
      'h',
      'i',
      'j',
      'k',
      'l',
      'm',
      'n',
      'o',
      'p',
      'q',
      'r',
      's',
      't',
      'u',
      'v',
      'w',
      'x',
      'y',
      'z',
      '+',
      '/'
        };
        private long _interno = 0;
        private string _internoStr = "0";
        private const long MaxBase64 = 1152921504606846976;

        private Base64(long valor, string base64str)
        {
            this._internoStr = base64str;
            this._interno = valor;
        }

        public override string ToString()
        {
            return this._internoStr;
        }

        public static Base64 ReadInt(long number)
        {
            if (number > 1152921504606846976L)
                throw new Exception("Numero demasiado grande");
            StringBuilder stringBuilder = new StringBuilder();
            int num = 63;
            for (int index1 = 0; index1 < 10; ++index1)
            {
                int index2 = (int)(number >> index1 * 6 & (long)num);
                stringBuilder.Insert(0, Base64.Signs[index2]);
            }
            char[] chArray = new char[1] { '0' };
            string base64str = stringBuilder.ToString().TrimStart(chArray);
            return new Base64(number, base64str);
        }
    }
}
