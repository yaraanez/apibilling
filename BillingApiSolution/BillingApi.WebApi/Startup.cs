using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Billing.Lib.Business;
using Billing.Lib.Business.Impl;
using BillingApi.Core;
using BillingApi.Core.Dosage;
using BillingApi.Core.Impl;
using BillingApi.Core.Impl.Dosage;
using BillingApi.DataAccess.Repositories;
using BillingApi.DataAccess.Repositories.Impl;
using BillingApi.DataAccess.UnitOfWork;
using BillingApi.WebApi.UnitOfWork;
using Generic.Api.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace BillingApi.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add(typeof(GenericExcepcionFilter)))
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            Func<IServiceProvider, Context> contextFunc = (serviceProvider) =>
            {
                DbContextOptionsBuilder<Context> builder = new DbContextOptionsBuilder<Context>();
                string connectionString = Configuration.GetConnectionString("AppContext");
                builder.UseSqlServer(connectionString);
                return new Context(builder.Options);
            };
            services.AddScoped<IBillingUnitOfWork, Context>(contextFunc);

            services.AddScoped<IServiceDosageFiscalDocumentCreDeb, ServiceDosageFiscalDocumentCreDeb>();
            services.AddScoped<IServiceDosageInvoice, ServiceDosageInvoice>();
            services.AddScoped<IServiceDosageFiscalDocumentCreDeb, ServiceDosageFiscalDocumentCreDeb>();
            services.AddScoped<IServiceDosageInvoice, ServiceDosageInvoice>();
            services.AddScoped<IBillingFactory, BillingFactory>();
            services.AddScoped<IServiceBilling, ServiceBilling>();
            services.AddScoped<IControlCodeGenerator, CodeGenerator7>();

            #region Repositories            
            services.AddScoped<IRepoSale, RepoSale>();
            services.AddScoped<IRepoInvoice, RepoInvoice>();
            services.AddScoped<IRepoBilling, RepoBilling>();
            services.AddScoped<IRepoParameter, RepoParameter>();
            #endregion

            #region Services           
            //services.AddScoped<IServiceSession, ServiceSession>();
            #endregion
            services.AddControllers();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
