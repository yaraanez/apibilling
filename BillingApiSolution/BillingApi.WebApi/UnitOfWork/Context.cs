﻿using BillingApi.DataAccess.Entities;
using BillingApi.DataAccess.Mapping;
using BillingApi.DataAccess.UnitOfWork;
using Generic.Data.UnitOfWork.Impl;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillingApi.WebApi.UnitOfWork
{
    public class Context : DbContextUnitOfWork, IBillingUnitOfWork
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<Dosage> Dosage { get; set; }
        public DbSet<EconomicActivityType> EconomicActivityType { get; set; }
        public DbSet<FiscalDocument> FiscalDocument { get; set; }
        public DbSet<FiscalDocumentCreDe> FiscalDocumentCreDe { get; set; }
        public DbSet<FiscalDocumentType> FiscalDocumentType { get; set; }
        public DbSet<IdentityDocumentType> IdentityDocumentType { get; set; }
        public DbSet<Sale> Sale { get; set; }
        public DbSet<SaleDetail> SaleDetail { get; set; }
        public DbSet<SaleDetailType> SaleDetailType { get; set; }
        public DbSet<SaleDiscount> SaleDiscount { get; set; }
        public DbSet<SaleLineDiscount> SaleLineDiscount { get; set; }
        public DbSet<SalePayment> SalePayment { get; set; }
        public DbSet<SaleState> SaleState { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DosageConfig());
            modelBuilder.ApplyConfiguration(new EconomicActivityTypeConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentCreDeConfig());
            modelBuilder.ApplyConfiguration(new FiscalDocumentTypeConfig());
            modelBuilder.ApplyConfiguration(new IdentityDocumentTypeConfig());
            modelBuilder.ApplyConfiguration(new SaleStateConfig());
            modelBuilder.ApplyConfiguration(new SaleConfig());
            modelBuilder.ApplyConfiguration(new SaleDetailConfig());
            modelBuilder.ApplyConfiguration(new SaleDetailTypeConfig());
            modelBuilder.ApplyConfiguration(new SalePaymentConfig());
            modelBuilder.ApplyConfiguration(new SaleDiscountConfig());
            modelBuilder.ApplyConfiguration(new SaleLineDiscountConfig());
        }
    }
}
