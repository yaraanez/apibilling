﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillingApi.Core;
using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Param;
using Generic.Api.Controllers;
using Generic.DataTransfer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BillingApi.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BillingController : AppControllerBase<IServiceBilling>
    {
        public BillingController(IServiceBilling mainService) : base(mainService)
        {
        }

        [Route("store")]
        [HttpPost]
        public ApiResponse<SaleDto> Store([FromBody] StoreSaleParam param)
        {
            return ProcessReponse(this._mainService.Store(param));
        }
    }
}
