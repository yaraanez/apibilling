﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories
{
    public interface IRepoParameter : IRepoBase
    {
        short GetInvoiceType();
        string GetCompanyAddress();

        string GetCompanyNit();
        string GetCompanyLeyend();
        string GetCompanyPhone();
        string GetQrLeyend();
    }
}
