﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories
{
    public interface IRepoBilling : IRepoBase
    {
        string GetFiscalDocumentTitle(short id);
    }
}
