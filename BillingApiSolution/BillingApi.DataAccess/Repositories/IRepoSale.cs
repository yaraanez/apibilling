﻿using BillingApi.DataAccess.Entities;
using BillingApi.Dto.DataTransfer;
using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories
{
    public interface IRepoSale : IRepoBase
    {
        Sale Store(SaleDto param);
        SaleDto GetById(long param);
    }
}
