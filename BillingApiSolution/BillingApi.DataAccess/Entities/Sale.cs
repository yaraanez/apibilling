﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class Sale
    {
        public long SaleId { get; set; }
        public string UniqueCode { get; set; }
        public string HashCode { get; set; }
        public int Period { get; set; }
        public long? OriginalSaleId { get; set; }
        public string OriginalSaleUniqueCode { get; set; }
        public int CashBoxId { get; set; }
        public int BranchOfficeId { get; set; }
        public int PointOfSaleId { get; set; }
        public int BranchOfficeSinId { get; set; }
        public string ClientId { get; set; }
        public short IdentityDocumentTypeId { get; set; }
        public string IdentityDocument { get; set; }
        public string IdentityDocumentComplement { get; set; }
        public string ClientFistName { get; set; }
        public string ClientLastName { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }
        public short EconomicActivityTypeId { get; set; }
        public short FiscalDocumentTypeId { get; set; }
        public DateTime EmissionDate { get; set; }
        public decimal TotalDiscountPercent { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal SubTotalAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DonationAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TaxPercent { get; set; }
        public decimal TotalTaxedAmount { get; set; }
        public short EmissionTypeId { get; set; }
        public string CashierId { get; set; }
        public string CashierUser { get; set; }
        public short SectorDocumentId { get; set; }
        public string TurnId { get; set; }
        public DateTime? ContingencyDate { get; set; }
        public string KeyOrigen { get; set; }       
        public string ExtraInfo { get; set; }
        public short State { get; set; }
        public DateTime? PostVoidDate { get; set; }
        public short? PostVoidMotiveId { get; set; }
        public string PostVoidOtherMotive { get; set; }
        public bool GenerateInvoice { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public short InvoiceTypeId { get; set; }

        public virtual Sale OriginalSale { get; set; }
        public virtual FiscalDocumentCreDe FiscalDocumentCreDe { get; set; }
        public virtual FiscalDocument FiscalDocument { get; set; }
        public virtual EconomicActivityType EconomicActivityType { get; set; }
        public virtual FiscalDocumentType FiscalDocumentType { get; set; }
        public virtual IdentityDocumentType IdentityDocumentType { get; set; }
        public virtual SaleState SaleState { get; set; }

        public virtual ICollection<SalePayment> SalePayments { get; set; }
        public virtual ICollection<SaleDiscount> SaleDiscounts { get; set; }
        public virtual ICollection<SaleDetail> SaleDetails { get; set; }
        public virtual ICollection<Sale> CreDeSale { get; set; }

        public Sale()
        {
            this.SalePayments = new HashSet<SalePayment>();
            this.SaleDiscounts = new HashSet<SaleDiscount>();
            this.SaleDetails = new  HashSet<SaleDetail>();
            this.CreDeSale = new HashSet<Sale>();
        }
    }
}
