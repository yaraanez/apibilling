﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class FiscalDocumentType
    {
        public short FiscalDocumentTypeId { get; set; }
        public string Name { get; set; }
        public string KeySin { get; set; }
        public string GroupKey { get; set; }
        public string Descript { get; set; }
        public string Title { get; set; }
        public short State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }
        public virtual ICollection<FiscalDocument> FiscalDocuments { get; set; }

        public FiscalDocumentType()
        {
            this.FiscalDocuments = new HashSet<FiscalDocument>();
            this.Sales = new HashSet<Sale>();
        }
    }
}
