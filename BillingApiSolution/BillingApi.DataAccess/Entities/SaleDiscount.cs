﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class SaleDiscount
    {
        public long SaleDiscountId { get; set; }
        public long SaleId { get; set; }
        public string DiscountId { get; set; }
        public string DiscountType { get; set; }
        public string Descript { get; set; }
        public decimal Percent { get; set; }
        public decimal Amount { get; set; }
        public string AuthUser { get; set; }
        public string AuthName { get; set; }
        public string ExtraInfo { get; set; }

        public virtual Sale Sale { get; set; }
    }
}
