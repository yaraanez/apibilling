﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class SaleDetailType
    {
        public short SaleDetailTypeId { get; set; }
        public string Name { get; set; }
        public string Descript { get; set; }
        public short State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual ICollection<SaleDetail> SaleDetails { get; set; }

        public SaleDetailType()
        {
            this.SaleDetails = new HashSet<SaleDetail>();
        }
    }
}
