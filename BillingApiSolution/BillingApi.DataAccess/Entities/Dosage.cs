﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class Dosage
    {
        public long AutorizationNumber { get; set; }
        public string Name { get; set; }
        public string DosageKey { get; set; }
        public DateTime DeadlineDate { get; set; }
        public short EconomicActivityId { get; set; }
        public string GroupKey { get; set; }
        public long InitialInvoiceNumber { get; set; }
        public long InvoiceNumber { get; set; }
        public string EconomicActivityLeyend { get; set; }
        public int Management { get; set; }
        public short State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual EconomicActivityType EconomicActivity { get; set; }
        public virtual ICollection<FiscalDocument> FiscalDocuments { get; set; }

        public Dosage()
        {
            this.FiscalDocuments = new HashSet<FiscalDocument>();
        }
    }
}
