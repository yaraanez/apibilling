﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using BillingApi.DataAccess.Entities;


namespace BillingApi.DataAccess.Mapping
{
    public class FiscalDocumentCreDeConfig : IEntityTypeConfiguration<FiscalDocumentCreDe>
    {
        public void Configure(EntityTypeBuilder<FiscalDocumentCreDe> builder)
        {
            builder
                .HasKey(c => c.SaleId);

            builder
                .Property(c => c.SaleId)
                .ValueGeneratedNever();
            builder
                .Property(c => c.OriginalUniqueCode)
                .HasMaxLength(25)
                .IsRequired(true);
            builder
                .Property(c => c.OriginalBranchOfficeId)               
                .IsRequired(true);
            builder
               .Property(c => c.OriginalPointOfSaleId)
               .IsRequired(true);
            builder
               .Property(c => c.OriginalCashboxId)
               .IsRequired(true);
            builder
               .Property(c => c.OriginalEmissionDate)
               .HasColumnType(string.Format($"datetime"))
               .IsRequired(true);
            builder
               .Property(c => c.OriginalInvoiceNumber)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalTotalTaxedAmount)
               .HasColumnType("decimal(20,2)")
               .IsRequired(true);
            builder
               .Property(c => c.OriginalAutorizationNumber)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalControlCode)
               .HasMaxLength(20)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalDosageKey)
               .HasMaxLength(150)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalCuf)
               .HasMaxLength(100)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalCufd)
               .HasMaxLength(100)
               .IsRequired(false);
            builder
               .Property(c => c.OriginalCuis)
               .HasMaxLength(100)
               .IsRequired(false);
        }
    }
}
