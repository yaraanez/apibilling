﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using BillingApi.DataAccess.Entities;

namespace BillingApi.DataAccess.Mapping
{
    public class DosageConfig : IEntityTypeConfiguration<Dosage>
    {
        public void Configure(EntityTypeBuilder<Dosage> builder)
        {
            builder
                .HasKey(c => c.AutorizationNumber);

            builder
                .Property(c => c.AutorizationNumber)
                .ValueGeneratedNever();

            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);
            builder
                .Property(c => c.DosageKey)
                .HasMaxLength(200)
                .IsRequired(false);
            builder
                .Property(c => c.DeadlineDate)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));
            builder
                .Property(c => c.EconomicActivityId)
                .IsRequired(true);
            builder
                .Property(c => c.GroupKey)
                .HasMaxLength(22)
                .IsRequired(true);
            builder
                .Property(c=> c.InitialInvoiceNumber)
                .IsRequired(true);
            builder
                .Property(c=> c.InvoiceNumber)
                .IsRequired(true);
            builder
              .Property(c => c.EconomicActivityLeyend)
              .HasMaxLength(500)
              .IsRequired(true);
            builder
              .Property(c => c.Management)
              .IsRequired(true);
            builder
              .Property(c => c.State)
              .IsRequired(true);
            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));

            builder
                .HasOne(c => c.EconomicActivity)
                .WithMany(c => c.Dosages)
                .HasForeignKey(c => c.EconomicActivityId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
