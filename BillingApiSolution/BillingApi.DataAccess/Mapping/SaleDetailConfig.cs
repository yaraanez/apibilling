﻿using BillingApi.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Mapping
{
    public class SaleDetailConfig : IEntityTypeConfiguration<SaleDetail>
    {
        public void Configure(EntityTypeBuilder<SaleDetail> builder)
        {
            builder
                  .HasKey(c => c.SaleDetailId);

            builder
                .Property(c => c.SaleDetailId)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.SaleId)
                .IsRequired(true);

            builder
                .Property(c => c.LineId)
                .IsRequired(true);

            builder
                .Property(c => c.ItemEconomicActivityId)
                .HasMaxLength(22)
                .IsRequired(false);

            builder
                .Property(c => c.ItemMeasurementUnitSin)
                .HasMaxLength(22)
                .IsRequired(false);

            builder
                .Property(c => c.ItemIdSin)
                .HasMaxLength(22)
                .IsRequired(true);

            builder
                .Property(c => c.ItemId)
                .HasMaxLength(22)
                .IsRequired(true);

            builder
                .Property(c => c.ItemDescript)
                .HasMaxLength(200)
                .IsRequired(true);

            builder
                .Property(c => c.ItemMeasurementUnit)
                .HasMaxLength(50)
                .IsRequired(true);

            builder
                .Property(c => c.ItemUnitPrice)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.Quantity)
                .IsRequired(true);

            builder
                .Property(c => c.SubTotalAmount)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.TotalDiscountPercent)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.TotalDiscount)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.TaxAmount)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.TaxPercent)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.TotalTaxedAmount)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.ReturnLineId)
                .IsRequired(false);

            builder
                .Property(c => c.Comments)
                .HasMaxLength(2000)
                .IsRequired(false);

            builder
                .Property(c => c.ExtraInfo)
                .IsRequired(false);
            builder
                .Property(c => c.SaleDetailTypeId)
                .IsRequired(true);

            builder
                .HasOne(c=> c.Sale)
                .WithMany(c=> c.SaleDetails)
                .HasForeignKey(c=> c.SaleId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.SaleDetailType)
                .WithMany(c => c.SaleDetails)
                .HasForeignKey(c => c.SaleDetailTypeId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
