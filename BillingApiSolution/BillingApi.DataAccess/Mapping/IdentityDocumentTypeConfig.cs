﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using BillingApi.DataAccess.Entities;

namespace BillingApi.DataAccess.Mapping
{
    public class IdentityDocumentTypeConfig : IEntityTypeConfiguration<IdentityDocumentType>
    {
        public void Configure(EntityTypeBuilder<IdentityDocumentType> builder)
        {
            builder
               .HasKey(c => c.IdentityDocumentTypeId);

            builder
                .Property(c => c.IdentityDocumentTypeId)
                .ValueGeneratedNever();

            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);
            builder
              .Property(c => c.KeySin)
              .HasMaxLength(10)
              .IsRequired(true);
            builder
              .Property(c => c.Descript)
              .HasMaxLength(255)
              .IsRequired(false);
            builder
              .Property(c => c.Title)
              .HasMaxLength(255)
              .IsRequired(false);
            builder
              .Property(c => c.State)
              .IsRequired(true);
            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
        }
    }
}
