﻿using BillingApi.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Mapping
{
    public class SalePaymentConfig : IEntityTypeConfiguration<SalePayment>
    {
        public void Configure(EntityTypeBuilder<SalePayment> builder)
        {
            builder
                  .HasKey(c => c.SalePaymentId);

            builder
                .Property(c => c.SalePaymentId)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.SaleId)
                .IsRequired(true);

            builder
                .Property(c => c.PaymentType)
                .HasMaxLength(50)
                .IsRequired(true);

            builder
              .Property(c => c.Amount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);

            builder
              .Property(c => c.Change)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);

            builder
                .Property(c => c.CardNumber)
                .HasMaxLength(40)
                .IsRequired(false);

            builder
                .Property(c => c.ConfirmationCode)
                .HasMaxLength(20)
                .IsRequired(false);

            builder
                .Property(c => c.ClientCode)
                .HasMaxLength(22)
                .IsRequired(false);

            builder
                .Property(c => c.BankCode)
                .HasMaxLength(22)
                .IsRequired(false);

            builder
                .Property(c => c.BankName)
                .HasMaxLength(200)
                .IsRequired(false);

            builder
                .Property(c => c.ExtraInfo)
                .IsRequired(false);

            builder
                .Property(c => c.AuthCode)
                .HasMaxLength(200)
                .IsRequired(false);

            builder
                .HasOne(c => c.Sale)
                .WithMany(c => c.SalePayments)
                .HasForeignKey(c => c.SaleId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
