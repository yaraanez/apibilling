﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Data.UnitOfWork
{
    public interface IUnitOfWorkBase : IDisposable
    {
        int SaveChanges();        
    }
}
