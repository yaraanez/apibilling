﻿using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Generic.DataTransfer.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Generic.Api.Filters
{
    public class GenericExcepcionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            ApiResponse<string> response = new ApiResponse<string>()
            {
                Codigo = (short)ApiResponseEnum.Error,
                Data = null,
                Errores = new List<string>(),
                Mensaje = context.Exception.Message
            };

            if (context.Exception is ValidationException)
            {
                ValidationException validationException = context.Exception as ValidationException;
                response.Errores = validationException.Errores;
            }
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = 200;
            context.Result = new JsonResult(response);
        }
    }
}
