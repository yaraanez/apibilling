﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Enum
{
    public static class EconomicActivityTypeEnum
    {
        public static short Farmacia = 1;
        public static short Sala = 2;
    }
}
