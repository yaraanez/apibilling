﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Enum
{
    public static class FiscalDocumentTypeEnum
    {
        public static short Factura = 1;
        public static short NotaCreditoDebito = 2;
        public static short NotaFiscal = 3;
        public static short BoletaContingencia = 4;
        public static short DocumentoEquivalente = 5;
    }
}
