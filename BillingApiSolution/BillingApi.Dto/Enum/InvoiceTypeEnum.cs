﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Enum
{
    public static class InvoiceTypeEnum
    {
        public static short Ninguna = 0;
        public static short Dosage = 1;
        public static short SFE = 2;
    }
}
