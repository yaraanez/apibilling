﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.DataTransfer
{
    public partial class FiscalDocumentDto
    {
        public long SaleId { get; set; }
        public long InvoiceNumber { get; set; }
        public DateTime EmissionDate { get; set; }
        public string ControlCode { get; set; }
        public string DosageKey { get; set; }
        public long? AutorizationNumber { get; set; }
        public DateTime? DeadlineEmissionDate { get; set; }
        public short? IdentityDocumentTypeId { get; set; }
        public string IdentityDocument { get; set; }
        public string IdentityComplement { get; set; }
        public string FirstClientName { get; set; }
        public string LastClientName { get; set; }
        public short FiscalDocumentTypeId { get; set; }
        public short EconomicActivityId { get; set; }
        public string Cuf { get; set; }
        public string Cufd { get; set; }
        public string Cuis { get; set; }
        public string CompanyLeyend { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyNit { get; set; }
        public string DocumentFiscalTitle { get; set; }
        public string QrLeyend { get; set; }
        public string QrBuffer { get; set; }
        public string EconomicActivityLeyend { get; set; }
        public string BranchOfficeLeyend { get; set; }
        public string FiscalDocumentLeyend { get; set; }
        public string InvoiceLeyend { get; set; }
        public string InvoiceStateLeyend { get; set; }
        public short State { get; set; }
        public DateTime? PostVoidDate { get; set; }
        public short? PostVoidMotiveId { get; set; }
    }
}
