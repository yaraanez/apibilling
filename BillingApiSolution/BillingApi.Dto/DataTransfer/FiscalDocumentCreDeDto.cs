﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.DataTransfer
{
    public partial class FiscalDocumentCreDeDto
    {
        public long SaleId { get; set; }
        public string OriginalUniqueCode { get; set; }
        public int OriginalBranchOfficeId { get; set; }
        public int OriginalPointOfSaleId { get; set; }
        public int OriginalCashboxId { get; set; }
        public DateTime OriginalEmissionDate { get; set; }
        public long? OriginalInvoiceNumber { get; set; }
        public decimal OriginalTotalTaxedAmount { get; set; }
        public decimal OriginalTotalAmount { get; set; }

        public long? OriginalAutorizationNumber { get; set; }
        public string OriginalControlCode { get; set; }
        public string OriginalDosageKey { get; set; }

        public string OriginalCuf { get; set; }
        public string OriginalCufd { get; set; }
        public string OriginalCuis { get; set; }
    }
}
