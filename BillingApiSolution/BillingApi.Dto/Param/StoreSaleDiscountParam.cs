﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Param
{
    public partial class StoreSaleDiscountParam
    {
        public string DiscountId { get; set; }
        public string DiscountType { get; set; }
        public string Descript { get; set; }
        public decimal Percent { get; set; }
        public decimal Amount { get; set; }
        public string AuthUser { get; set; }
        public string AuthName { get; set; }
        public string ExtraInfo { get; set; }
    }
}
