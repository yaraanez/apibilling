﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Param
{
    public partial class StoreSaleDetailParam
    {
        public int LineId { get; set; }
        public string ItemEconomicActivityId { get; set; }
        public string ItemMeasurementUnitSin { get; set; }
        public string ItemIdSin { get; set; }
        public string ItemId { get; set; }
        public string ItemDescript { get; set; }
        public string ItemMeasurementUnit { get; set; }
        public decimal ItemUnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal SubTotalAmount { get; set; }
        public decimal TotalDiscountPercent { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TaxPercent { get; set; }
        public decimal TotalTaxedAmount { get; set; }
        public int? ReturnLineId { get; set; }
        public string Comments { get; set; }
        public string ExtraInfo { get; set; }
        public short SaleDetailTypeId { get; set; }
        public virtual List<StoreSaleDiscountParam> SaleLineDiscount { get; set; }
    }
}
