﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Param
{
    public class IncInvoiceDosageNumberParam
    {
        public long AutorizationNumber { get; set; }
        public long InvoiceNumber { get; set; }
    }
}
