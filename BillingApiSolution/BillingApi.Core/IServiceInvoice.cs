﻿using BillingApi.Dto.DataTransfer;
using Generic.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Core
{
    public interface IServiceInvoice : IServiceBase
    {
        short InvoiceType { get; }
        void GenerateBillingObjects(SaleDto param);
        string GenerateQrBuffer(SaleDto param, FiscalDocumentDto fiscalDocumentDto);
    }
}
