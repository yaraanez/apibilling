﻿using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Param;
using Generic.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Core
{
    public interface IServiceFiscalDocumentCreDeb : IServiceBase
    {
        void ProcessFiscalDocumentCreDeb(StoreSaleParam param, SaleDto saleDto);
    }
}
