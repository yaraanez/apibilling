﻿using Billing.Lib.Business;
using Billing.Lib.DataTransfer;
using Billing.Lib.Utils;
using BillingApi.Core.Dosage;
using BillingApi.DataAccess.Repositories;
using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Enum;
using BillingApi.Dto.Param;
using Generic.Core.Services.Impl;
using Generic.Data;
using Generic.DataTransfer.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace BillingApi.Core.Impl.Dosage
{
    public class ServiceDosageInvoice : ServiceBase<IRepoInvoice>, IServiceDosageInvoice
    {
        public static Dictionary<long, object> tolock = new Dictionary<long, object>();
        private readonly IControlCodeGenerator _controlCodeGenerator;
        private readonly IRepoParameter _repoParameter;

        public ServiceDosageInvoice(
            IControlCodeGenerator controlCodeGenerator, 
            IRepoInvoice mainRepo,
            IRepoParameter repoParameter) : base(mainRepo)
        {
            this._repoParameter = repoParameter;
            this._controlCodeGenerator = controlCodeGenerator;
        }

        public short InvoiceType => InvoiceTypeEnum.Dosage;

        public void GenerateBillingObjects(SaleDto param)
        {            
            if (param.TotalTaxedAmount == 0 && param.GenerateInvoice)
            {
                param.GenerateInvoice= false;
            }
            if (param.GenerateInvoice)
            {
                DosageDto dosageDto = this._mainRepo.GetEnableDosage(new GetEnableDosageParam()
                {
                    EmissionDate = param.EmissionDate,
                    EconomicActivityTypeId = param.EconomicActivityTypeId,
                    FiscalDocumentTypeId = param.FiscalDocumentTypeId
                });
                if (!tolock.ContainsKey(dosageDto.AutorizationNumber))
                {
                    tolock.Add(dosageDto.AutorizationNumber, new object());
                }
                lock (tolock[dosageDto.AutorizationNumber])
                {
                    string controlCode = this._controlCodeGenerator.GenerateCode(new GenerateCodeParams()
                    {
                        CiNitClient = TextUtils.ToInt(param.IdentityDocument),
                        InvoiceNumber = dosageDto.InvoiceNumber,
                        Key = dosageDto.DosageKey,
                        AuthorizationNumber = dosageDto.AutorizationNumber,
                        AmountDouble = param.TotalTaxedAmount,
                        TransactionDateTime = param.EmissionDate
                    }, true);

                    controlCode = TextUtils.SINFormat(controlCode);

                    param.FiscalDocument = new FiscalDocumentDto()
                    {
                        IdentityDocument = param.IdentityDocument,
                        ControlCode = controlCode,
                        InvoiceNumber = dosageDto.InvoiceNumber,
                        AutorizationNumber = dosageDto.AutorizationNumber,
                        FirstClientName = param.ClientFistName,
                        LastClientName = param.ClientLastName,
                        FiscalDocumentTypeId = param.FiscalDocumentTypeId,
                        IdentityDocumentTypeId = param.IdentityDocumentTypeId,
                        IdentityComplement = param.IdentityDocumentComplement,
                        CompanyAddress = null,
                        BranchOfficeLeyend = null,
                        State = StateEnum.Enabled,
                        EmissionDate = param.ContingencyDate ?? param.EmissionDate,
                        PostVoidDate = null,
                        PostVoidMotiveId = null,
                        DosageKey = dosageDto.DosageKey,
                        EconomicActivityId = param.EconomicActivityTypeId,
                        SaleId = 0,
                        Cufd = null,
                        Cuis = null,
                        Cuf = null,
                        CompanyLeyend = null,
                        CompanyNit = null,
                        CompanyPhone = null,
                        DeadlineEmissionDate = dosageDto.DeadlineDate,
                        QrBuffer = null,
                        QrLeyend = this._repoParameter.GetQrLeyend(),
                        InvoiceStateLeyend = null,
                        DocumentFiscalTitle = null,
                        FiscalDocumentLeyend = null,
                        InvoiceLeyend = this._mainRepo.GetEconomicActivityLeyend(param.EconomicActivityTypeId),
                        EconomicActivityLeyend = dosageDto.EconomicActivityLeyend,
                    };
                    param.State = SaleStateEnum.Facturada;
                    param.InvoiceTypeId= InvoiceTypeEnum.Dosage;

                    dosageDto.InvoiceNumber++;

                    this._mainRepo.IncInvoiceDosageNumber(new IncInvoiceDosageNumberParam()
                    {
                        AutorizationNumber = dosageDto.AutorizationNumber,
                        InvoiceNumber = dosageDto.InvoiceNumber
                    });
                    this._mainRepo.SaveChanges();
                }
            }
        }

        public string GenerateQrBuffer(SaleDto param, FiscalDocumentDto fiscalDocumentDto)
        {
            string separador = "|";
            string sImporteICE = "0";
            string sImporteVentasNG = "0";
            string sImporteNoSujetoCredFiscal = "0";

            StringBuilder response = new StringBuilder();
            response.Length = 0;
            response.Capacity = 0;
            response.Append(fiscalDocumentDto.CompanyNit);                                                  //1. Nit Emisor
            response.Append(separador).Append(fiscalDocumentDto.InvoiceNumber);                        //2. Número de factura
            response.Append(separador).Append(fiscalDocumentDto.AutorizationNumber);                   //3. Número de autorización
            response.Append(separador).Append(param.EmissionDate.ToString(Utils.DateTimeDDMMYYYYFormat));               //4. Fecha de Emisión
            response.Append(separador).Append(param.TotalTaxedAmount.ToString(Utils.DecimalPointDecimalFormat, CultureInfo.CreateSpecificCulture("en-US")));  //5. Monto total consignado en la factura
            response.Append(separador).Append(param.TotalTaxedAmount.ToString(Utils.DecimalPointDecimalFormat, CultureInfo.CreateSpecificCulture("en-US")));  //6. Monto válido para crédito fiscal
            response.Append(separador).Append(fiscalDocumentDto.ControlCode);                        //7. Código de control
            response.Append(separador).Append(param.IdentityDocument);                            //8. Nit del comprador
            response.Append(separador).Append(sImporteICE);                                             //9. Importe ICE, IEHD, TASAS
            response.Append(separador).Append(sImporteVentasNG);                                        //10 Importe por ventas no grabadas o a tasas cero
            response.Append(separador).Append(sImporteNoSujetoCredFiscal);                              //11 Importe no sujeto a crédito fiscal
            response.Append(separador).Append(param.TotalDiscountAmount.ToString(Utils.DecimalPointDecimalFormat, CultureInfo.CreateSpecificCulture("en-US")));              //12 Descuentos, bonificaciones o rebajas
            return response.ToString();
        }
    }
}
