﻿using BillingApi.Core.Dosage;
using BillingApi.Dto.Enum;
using Generic.Core.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Core.Impl
{
    public partial class BillingFactory : ServiceBase, IBillingFactory
    {
        private readonly IServiceDosageInvoice _serviceDosageInvoice;
        private readonly IServiceDosageFiscalDocumentCreDeb _serviceDosageFiscalDocumentCreDeb;

        public BillingFactory(
            IServiceDosageInvoice serviceDosageInvoice,
            IServiceDosageFiscalDocumentCreDeb serviceDosageFiscalDocumentCreDeb)
        {
            this._serviceDosageInvoice = serviceDosageInvoice;
            this._serviceDosageFiscalDocumentCreDeb = serviceDosageFiscalDocumentCreDeb;
        }

        public override void Dispose()
        {
            this._serviceDosageInvoice?.Dispose();
            this._serviceDosageFiscalDocumentCreDeb?.Dispose();
        }

        public IServiceInvoice GetBillingService(short invoiceType)
        {
            IServiceInvoice service = null;
            if (invoiceType == InvoiceTypeEnum.Dosage)
            {
                service = this._serviceDosageInvoice;
            }
            else if (invoiceType == InvoiceTypeEnum.SFE)
            {
                
            }
            return service;
        }

        public IServiceFiscalDocumentCreDeb GetFiscalDocumentCreDeService(short invoiceType)
        {
            IServiceFiscalDocumentCreDeb service = null;
            if (invoiceType == InvoiceTypeEnum.Dosage)
            {
                service = this._serviceDosageFiscalDocumentCreDeb;
            }
            else if (invoiceType == InvoiceTypeEnum.SFE)
            {

            }
            return service;
        }
    }
}
