﻿using BillingApi.DataAccess.Repositories;
using BillingApi.DataAccess.Repositories.Impl;
using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Enum;
using BillingApi.Dto.Param;
using Generic.Core.Services.Impl;
using Generic.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using BillingApi.DataAccess.Entities;

namespace BillingApi.Core.Impl
{
    public partial class ServiceBilling : ServiceBase<IRepoBilling>, IServiceBilling
    {
        private readonly IRepoParameter _repoParameter;
        private readonly IBillingFactory _billingFactory;
        private IServiceInvoice _serviceInvoice;
        private readonly IRepoSale _repoSale;
        private IServiceFiscalDocumentCreDeb _serviceFiscalDocumentCreDeb;

        public ServiceBilling(
            IBillingFactory billingFactory,
            IRepoParameter repoParameter,
            IRepoSale repoSale,
            IRepoBilling mainRepo) : base(mainRepo)
        {
            this._billingFactory = billingFactory;
            this._repoParameter = repoParameter;
            this._repoSale = repoSale;
        }

        public SaleDto Store(StoreSaleParam param)
        {
            ProcessValidations(
                (messages) =>
                {
                    //El campo "Fecha de Contingencia" es requerido.
                    if (param.EmissionTypeId == EmissionTypeEnum.Offline)
                    {
                        if (!param.ContingencyDate.HasValue)
                        {
                            messages.Add("El campo \"Fecha de Contingencia\" es requerido.");
                        }
                        //La Fecha de Contingencia "{0}" debe ser menor a la Fecha de Emisón "{1}".                            
                        else if (param.ContingencyDate.Value >= param.EmissionDate)
                        {
                            messages.Add(string.Format("La Fecha de Contingencia \"{0}\" debe ser menor a la Fecha de Emisón \"{1}\".", param.ContingencyDate.Value.ToString(Utils.DateTimeDDMMYYYYHHMMSSFormat), param.EmissionDate.ToString(Utils.DateTimeDDMMYYYYHHMMSSFormat)));
                        }
                    }

                    foreach (var line in param.SaleDetail)
                    {
                        //El campo "Precio" de la linea "{0}" no debe ser igual a 0.
                        if (line.ItemUnitPrice == 0)
                        {
                            messages.Add(string.Format("El campo \"Precio\" de la linea \"{0}\" no debe ser igual a 0.", line.LineId));
                        }
                        //El campo "Cantidad" de la linea "{0}" no debe ser igual a 0.
                        if (line.Quantity == 0)
                        {
                            messages.Add(string.Format("El campo \"Cantidad\" de la linea \"{0}\" no debe ser igual a 0.", line.LineId));
                        }
                    }

                    //La combinación de tipo de documento fiscal "{0}" con tipo de documento de sector "{1}" no es permitida.
                    if (!((param.FiscalDocumentTypeId == FiscalDocumentTypeEnum.Factura && param.SectorDocumentId == SectorDocumentTypeEnum.FacturaStandard)
                    || (param.FiscalDocumentTypeId == FiscalDocumentTypeEnum.NotaCreditoDebito && param.SectorDocumentId == SectorDocumentTypeEnum.NotaCreditoDebito)
                    || (param.FiscalDocumentTypeId == FiscalDocumentTypeEnum.BoletaContingencia && param.SectorDocumentId == SectorDocumentTypeEnum.FacturaStandard)
                    || (param.FiscalDocumentTypeId == FiscalDocumentTypeEnum.NotaFiscal && param.SectorDocumentId == SectorDocumentTypeEnum.NotaFiscalZonaFranca)
                    || (param.FiscalDocumentTypeId == FiscalDocumentTypeEnum.BoletaContingencia && param.SectorDocumentId == SectorDocumentTypeEnum.NotaFiscalZonaFranca)))
                    {
                        messages.Add(string.Format("La combinación de tipo de documento fiscal \"{0}\" con tipo de documento de sector \"{1}\" no es permitida.", param.FiscalDocumentTypeId, param.SectorDocumentId));
                    }
                });
            short invoiceType = this._repoParameter.GetInvoiceType();
            this._serviceInvoice = this._billingFactory.GetBillingService(invoiceType);
            this._serviceFiscalDocumentCreDeb = this._billingFactory.GetFiscalDocumentCreDeService(invoiceType);

            string hash = Utils.SHA256Encrypt(JsonSerializer.Serialize(param));

            SaleDto saleDto = new SaleDto()
            {
                CashierUser = param.CashierUser,
                BranchOfficeId = param.BranchOfficeId,
                BranchOfficeSinId = param.BranchOfficeSinId,
                CashBoxId = param.CashBoxId,
                CashierId = param.CashierId,
                ClientEmail = param.ClientEmail,
                ClientFistName = param.ClientFistName,
                ClientId = param.ClientId,
                ClientLastName = param.ClientLastName,
                ClientPhone = param.ClientPhone,
                ContingencyDate = param.ContingencyDate,
                CreateDate = DateTime.Now,
                DonationAmount = param.DonationAmount,
                EconomicActivityTypeId = param.EconomicActivityTypeId,
                EditDate = DateTime.Now,
                EmissionDate = param.EmissionDate,
                EmissionTypeId = param.EmissionTypeId,
                ExtraInfo = param.ExtraInfo,
                FiscalDocumentTypeId = param.FiscalDocumentTypeId,
                HashCode = hash,
                InvoiceTypeId = param.InvoiceTypeId,
                KeyOrigen = param.KeyOrigen,
                IdentityDocument = param.IdentityDocument,
                OriginalSaleId = null,
                OriginalSaleUniqueCode = param.OriginalSaleUniqueCode,
                Period = DateTime.Now.Year,
                GenerateInvoice = param.GenerateInvoice,
                PointOfSaleId = param.PointOfSaleId,
                PostVoidDate = null,
                PostVoidMotiveId = null,
                IdentityDocumentTypeId = param.IdentityDocumentTypeId,
                SaleId = 0,
                State = SaleStateEnum.Facturada,
                IdentityDocumentComplement = param.IdentityDocumentComplement,
                SectorDocumentId = param.SectorDocumentId,
                TaxAmount = param.TaxAmount,
                TaxPercent = param.TaxPercent,
                TotalAmount = param.TotalAmount,
                TotalDiscountAmount = param.TotalDiscountAmount,
                TotalDiscountPercent = param.TotalDiscountPercent,
                TurnId = param.TurnId,
                UniqueCode = param.UniqueCode,
                TotalTaxedAmount = param.TotalTaxedAmount,
                SubTotalAmount = param.SubTotalAmount,
                SalePayment = param.SalePayment.Select(p => new SalePaymentDto()
                {
                    Amount = p.Amount,
                    AuthCode = p.AuthCode,
                    BankCode = p.BankCode,
                    BankName = p.BankName,
                    CardNumber = p.CardNumber,
                    Change = p.Change,
                    ClientCode = p.ClientCode,
                    ConfirmationCode = p.ConfirmationCode,
                    ExtraInfo = p.ExtraInfo,
                    PaymentType = p.PaymentType,
                    SaleId = 0,
                    SalePaymentId = 0
                }).ToList(),                                
                SaleDetail = param.SaleDetail.Select(x => new SaleDetailDto()
                {
                    SaleId = 0,
                    Comments = x.Comments,
                    ExtraInfo = x.ExtraInfo,
                    ItemDescript = x.ItemDescript,
                    ItemEconomicActivityId = x.ItemEconomicActivityId,
                    ItemId = x.ItemId,
                    ItemIdSin = x.ItemIdSin,
                    ItemMeasurementUnit = x.ItemMeasurementUnit,
                    ItemMeasurementUnitSin = x.ItemMeasurementUnitSin,
                    ItemUnitPrice = x.ItemUnitPrice,
                    LineId = x.LineId,
                    Quantity = x.Quantity,
                    ReturnLineId = x.ReturnLineId,
                    SaleDetailId = 0,
                    SaleDetailTypeId = x.SaleDetailTypeId,
                    SubTotalAmount = x.SubTotalAmount,
                    TaxAmount = x.TaxAmount,
                    TaxPercent = x.TaxPercent,
                    TotalAmount = x.TotalAmount,
                    TotalDiscount = x.TotalDiscount,
                    TotalDiscountPercent = x.TotalDiscountPercent,
                    TotalTaxedAmount = x.TotalTaxedAmount,
                    SaleLineDiscount = x.SaleLineDiscount.Select(y => new SaleLineDiscountDto()
                    {
                        Amount = y.Amount,
                        AuthName = y.AuthName,
                        AuthUser = y.AuthUser,
                        Descript = y.Descript,
                        DiscountId = y.DiscountId,
                        DiscountType = y.DiscountType,
                        ExtraInfo = y.ExtraInfo,
                        Percent = y.Percent,
                        SaleDetailId = 0,
                        SaleLineDiscountId = 0,
                    }).ToList(),
                }).ToList(),
                SaleDiscount = param.SaleDiscount.Select(x => new SaleDiscountDto()
                {
                    Amount = x.Amount,
                    AuthName = x.AuthName,
                    AuthUser = x.AuthUser,
                    Descript = x.Descript,
                    DiscountId = x.DiscountId,
                    DiscountType = x.DiscountType,
                    ExtraInfo = x.ExtraInfo,
                    Percent = x.Percent,
                    SaleDiscountId = 0,
                    SaleId = 0
                }).ToList()
            };

            //Proceso de documento crédito / debito
            this._serviceFiscalDocumentCreDeb.ProcessFiscalDocumentCreDeb(param, saleDto);

            //Proceso de facturación
            this._serviceInvoice.GenerateBillingObjects(saleDto);

            if (saleDto.FiscalDocument != null)
            {
                saleDto.FiscalDocument.CompanyAddress = this._repoParameter.GetCompanyAddress();
                saleDto.FiscalDocument.CompanyNit = this._repoParameter.GetCompanyNit();
                saleDto.FiscalDocument.CompanyLeyend = this._repoParameter.GetCompanyLeyend();
                saleDto.FiscalDocument.CompanyPhone = this._repoParameter.GetCompanyPhone();
                saleDto.FiscalDocument.QrBuffer = this._serviceInvoice.GenerateQrBuffer(saleDto, saleDto.FiscalDocument);
                saleDto.FiscalDocument.DocumentFiscalTitle = this._mainRepo.GetFiscalDocumentTitle(saleDto.FiscalDocumentTypeId);
            }

            //Guardado de venta
            Sale sale = this._repoSale.Store(saleDto);

            this._repoSale.SaveChanges();

            return saleDto;
        }
    }
}
