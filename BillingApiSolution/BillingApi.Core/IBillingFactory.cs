﻿using Generic.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Core
{
    public interface IBillingFactory : IServiceBase
    {
        IServiceInvoice GetBillingService(short invoiceType);
        IServiceFiscalDocumentCreDeb GetFiscalDocumentCreDeService(short invoiceType);
    }
}
