# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Proyecto Api, Componente de Facturación mediante dosificación
* Version 1.0.0

### How do I get set up? ###

* Cargar la solución en Visual Studio 2019, ingresar a la rama develop, ~apibilling\BillingApiSolution\
* Debe tener instalado .Net core 3.0
* .Net core 3.0
* La configuración esta en la ubicación ~apibilling\BillingApiSolution\BillingApi.WebApi\appsettings.json
* Ejecutar el proyecto MigrationApp, donde se debe configurar el archivo Context -> OnConfiguring, cadena de conección a la base de datos.
  La ejecución de este proyecto creará el contexto y aplicara las migraciónes de ser necesario.
* Para realizar la prueba, se debe correr el proyecto BillingApi.WebApi, con el perfil IIS.
* Cargar el archivo ~apibilling\My API.postman_collection.json, para realizar la prueba mediante postman
* Configurar la url en las variables del proyecto Postman.

### Who do I talk to? ###

* Gonzalo Ernesto Pérez Morón.
* Yara Marianita Añez Aracu.